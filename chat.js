
async function iniciarchat(){
    const key="c2edc71bf0314d1b96fad093794d8c28"
    const question= (document.getElementById('btn-input').value === "" ) ? "hola" : document.getElementById('btn-input').value 
    const id ='a20f91fa-2f90-4226-b056-7b5ae89e0966'
    const URL=`https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/${id}?verbose=true&timezoneOffset=-360&subscription-key=${key}&q=${question}`
    const data_luis= await axios.get(URL)
    const intents = data_luis.data.topScoringIntent.intent
    const score = data_luis.data.topScoringIntent.score
  console.log(intents)
  console.log(score)
        let html=''
        const keyQnA='189a6ed8-aefc-471f-8c78-d90c657ab92f'
        const URLQnA = `https://qnatestingprueba.azurewebsites.net/qnamaker/knowledgebases/${keyQnA}/generateAnswer`
        const Authorizationkey='EndpointKey 17f7bc60-e0dc-494e-989f-bb116f53e46d'
        if(score >= 0.75 && intents !== "None"){
                let settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": `${URLQnA}`,
                    "method": "POST",
                    "headers": {
                      "Authorization": `${Authorizationkey}`,
                      "Content-Type": "application/json",
                    },
                    "processData": false,
                    "data": `{"question":"${intents}"}`
                  }
                  
                  $.ajax(settings).done(function (response) {
                    html = Message(2,response.answers[0].answer);
                    document.getElementById("chat").innerHTML +=html
                  });
            
        }else if(score <= 0.74   || intents==="None"){
            let settings = {
                "async": true,
                "crossDomain": true,
                "url": `${URLQnA}`,
                "method": "POST",
                "headers": {
                  "Authorization": `${Authorizationkey}`,
                  "Content-Type": "application/json",
                },
                "processData": false,
                "data": `{"question":"${intents}"}`
              }
              
              $.ajax(settings).done(function (response) {
                html = Message(2,response.answers[0].answer);
                document.getElementById("chat").innerHTML +=html
              });
        }
      }
    
function Message(type,respuesta){
        let img=''
        let position=''
        let name=''
        let question= ''
        if(type===1){
             img=`http://placehold.it/50/FA6F57/fff&text=ME`
             position=`right`
             name=`Giovanni Morais Reyna`
             question=document.getElementById('btn-input').value;
        }else {
            img=`http://placehold.it/50/55C1E7/fff&text=IA`
            position=`left` 
            name=`Luis IA`
            question=respuesta
        }
        
        const html=`
        <li class="${position} clearfix"><span class="chat-img pull-${position}">
                            <img src="${img}" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span></small>
                                    <strong class="pull-${position} primary-font">${name}</strong>
                                </div>
                                <br>
                                    ${question}
                            </div>
                        </li>
        `
        if(type===1){
            document.getElementById("chat").innerHTML +=html
            iniciarchat()
        }
        
        return  html
}

function SelectButton(user){
  let message=`has seleccionado ${user}`
  const menssageButton=Message(2,message);
  document.getElementById("chat").innerHTML +=menssageButton
}